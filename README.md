# README #

This repository is intended to explain how to setup webb/app servers. 

# SETUP A MINIMALIST APP SERVER WITH SINATRA #
Go to simple_sinatra_framework folder [link](https://bitbucket.org/abelbalbis/pi_server/src/b66e13b26d0fbfe0e66026a88ebc6fce5e66c5c6/simple_sinatra_framework/?at=master)
# SETUP A POWERFUL WEB SERVER WITH PASSENGER+NGINX #
Go to power_nginx folder  [link](https://bitbucket.org/abelbalbis/pi_server/src/e9864ddb0759276c18988ad10e639c462b35caec/power_nginx/?at=master)




### Who do I talk to? ###

* Abel Balbis Calviño [mail](mailto:abelbalbis@gmail.com)