# README #
This tutorial will explain how to setup a powerful engine to create apps within an RaspberryPi.

## Prerequisites ##
To follow this tutorial ruby gem package manager is needed. If not installed yet on your raspbian distribution, do the following:

```
#!bash

pi@raspberrypi ~ $ sudo apt-get install rubygems
```
## Install Passenger/Nginx ##

```
#!bash

pi@raspberrypi ~ $ sudo gem install passenger
```

```
#!bash

pi@raspberrypi ~ $ sudo passenger-install-nginx-module
```

This will guide you trhough a installatiion process where you can select your preferred choices. For instnce you can install support the tools needed for your preferred tools such as ruby or nodejs:

![Captura de pantalla 2015-04-03 a las 15.21.33.png](https://bitbucket.org/repo/MRaGLL/images/1041582950-Captura%20de%20pantalla%202015-04-03%20a%20las%2015.21.33.png)


## Start server ##

Go to the instalation folder (by default /opt/nginx but you can change this in the instalation process):

```
#!bash

pi@raspberrypi ~ $ cd /opt/nginx/sbin
```
Start the server:
```
#!bash

pi@raspberrypi ~ $ sudo ./nginx
```

Your server now is up and running. You can go to http://localhost (from your raspberry) or to http://your-raspberry-local-ip and check that the server is properly working:

![Captura de pantalla 2015-04-03 a las 15.32.55.png](https://bitbucket.org/repo/MRaGLL/images/1578015975-Captura%20de%20pantalla%202015-04-03%20a%20las%2015.32.55.png)

To reload the server (from /opt/nginx/sbin folder), very useful when you change the contents in /opt/nginx/html folder:
```
#!bash

pi@raspberrypi ~ $ sudo ./nginx -s reload
```

To stop the server (from /opt/nginx/sbin folder):
```
#!bash

pi@raspberrypi ~ $ sudo ./nginx -s quit
```