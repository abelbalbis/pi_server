# README #

This repository is intended to explain how to setup a simple web server with ruby and [Sinatra server](http://www.sinatrarb.com/).

# SETUP RUBY #

Raspberry pi already has installed ruby, now we have to install the package manager:
### Install Ruby Gems ###
```
#!bash

pi@raspberrypi ~ $ sudo apt-get install rubygems
```



# SETUP SINATRA SERVER #

### Install Sinatra ###
```
#!bash

pi@raspberrypi ~ $ sudo gem install sinatra
```

### Clone this repository ###
```
#!bash

pi@raspberrypi ~ $ git clone https://abelbalbis@bitbucket.org/abelbalbis/pi_server.git
pi@raspberrypi ~ $ cd pi_server
```

### Run the server ###

```
#!bash
pi@raspberrypi ~ $ ruby hello_pi.rb -o 0.0.0.0
```
![Captura de pantalla 2015-03-25 a las 23.46.12.png](https://bitbucket.org/repo/MRaGLL/images/1854598650-Captura%20de%20pantalla%202015-03-25%20a%20las%2023.46.12.png)

### ENJOY ###
Now you can open the navigator of the raspberry pi and type:
```
#!bash
http://localhost:8080
```
![Captura de pantalla 2015-03-28 a las 18.48.04.png](https://bitbucket.org/repo/MRaGLL/images/4142791624-Captura%20de%20pantalla%202015-03-28%20a%20las%2018.48.04.png)

If you are trying to access from a local network, make sure that the port 8080 is open for the IP address of the raspberry pi, then you can access your favourite browser and have fun. 

```
#!bash
http://your-raspberrypi-local-IP-address:8080
```


### Who do I talk to? ###

* Abel Balbis Calviño